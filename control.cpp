#include "control.h"
#include <Arduino.h>
#include "motor.h"
#include "data.h"
#include "constant.hpp"

t_position nextPosition = nullPosition;



void setup_control(){
  Serial.println("INIT - CONTROL");
  globe_set_speed(TRAVEL_SPEED_GLOBE_STEP_SEC);
  sat_set_speed(TRAVEL_SPEED_SAT_STEP_SEC); 
}

void run_control(){
  if (nextPosition.timestamp_ms == nullPosition.timestamp_ms){
    nextPosition = popPosition();
  }
  if (nextPosition.timestamp_ms == nullPosition.timestamp_ms){
    globe_set_speed(0);
    sat_set_speed(0);
  } else {
    unsigned long const& timestamp_ms = getTimestamp_ms();
    if ( timestamp_ms > nextPosition.timestamp_ms ){
      nextPosition = nullPosition;
    } else {
      float duration_ms = nextPosition.timestamp_ms - timestamp_ms;

      float actual_sat_lat = sat_get_position() * SAT_RAD_BY_STEP_LAT + REF_LATITUDE;
      float diff_lat_rad = actual_sat_lat - (nextPosition.latitude_rad + PI);
      int sat_speed = ( diff_lat_rad / SAT_RAD_BY_STEP_LAT ) * (1000.0 / duration_ms);
      sat_set_speed( sat_speed );

      int sat_long_speed = sat_speed * SAT_RAD_BY_STEP_LONG;


      float actual_globe_long = globe_get_position() * GLOBE_RAD_BY_STEP + REF_LONGITUDE;
      float diff_long_rad = actual_globe_long - (nextPosition.longitude_rad + PI);
      int globe_speed = ( diff_long_rad / GLOBE_RAD_BY_STEP ) * (1000.0 / duration_ms);
      globe_set_speed( globe_speed - sat_long_speed );

      
    }    
  }
}
