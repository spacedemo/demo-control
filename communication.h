#ifndef COMMUNICATION_H
#define COMMUNICATION_H

#include "constant.hpp"

void setup_comm();

void run_comm();

void execute_command( const char* command );

#endif
