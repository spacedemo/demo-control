FROM northfuse/arduino-cli-docker

RUN mkdir demo-control
COPY ./*.cpp demo-control/
COPY ./*.h demo-control/
COPY ./*.hpp demo-control/
COPY ./*.ino demo-control/

RUN arduino-cli core update-index
RUN arduino-cli core install arduino:avr

RUN arduino-cli lib install "ArduinoThread"
RUN arduino-cli lib install "AccelStepper"
RUN arduino-cli lib install "Adafruit Motor Shield library"

RUN arduino-cli compile --fqbn arduino:avr:uno demo-control