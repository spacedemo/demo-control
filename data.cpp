#include "data.h"
#include <Arduino.h>

long offset_ms = 0;

void setOffset_ms(long const& value){
  offset_ms = value;
}
long getOffset_ms(){
  return offset_ms;
}


unsigned long timestamp_ms = 0;

void setTimestamp_ms(unsigned long const& value_ms){
  timestamp_ms = value_ms - offset_ms - millis();
}

unsigned long getTimestamp_ms(){
  return timestamp_ms + millis();
}


const int positionFifoLength = 10;
t_position positionFifo[positionFifoLength];
int positionFifoHead = 0;
int positionFifoTail = 0;


void pushPosition(t_position const& position){
   positionFifo[positionFifoHead] = position;
   positionFifoHead++;
   if (positionFifoHead >= positionFifoLength)
      positionFifoHead = 0;
}

t_position popPosition(){
  if (positionFifoTail == positionFifoHead)
    return nullPosition;
  t_position value = positionFifo[positionFifoTail];
  positionFifoTail++;
  if (positionFifoTail >= positionFifoLength)
    positionFifoTail = 0;
}
