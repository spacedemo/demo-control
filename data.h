#ifndef DATA_H
#define DATA_H

#include "constant.hpp"

typedef struct {
  float latitude_rad;
  float longitude_rad;
  unsigned long timestamp_ms;
} t_position;


static t_position nullPosition = {
  0.0,
  0.0,
  0
};


void setOffset_ms(long const& value_ms);
long getOffset_ms();

void setTimestamp_ms(unsigned long const& value_ms);
unsigned long getTimestamp_ms();

void pushPosition(t_position const& position);
t_position popPosition();



#endif
