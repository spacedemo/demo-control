#include "switches.h"
#include <Arduino.h>
#include "data.h"
#include "motor.h"

boolean globe_switch_state, sat_switch_state, emergency_stop_state;

void setup_switches(){
  pinMode(GLOBE_SWITCH, INPUT);  
  pinMode(SAT_SWITCH, INPUT); 
  pinMode(EMERGENCY_STOP, INPUT_PULLUP); 

  globe_switch_state = digitalRead(GLOBE_SWITCH);
  sat_switch_state = digitalRead(SAT_SWITCH);
  emergency_stop_state = digitalRead(EMERGENCY_STOP);
  
  Serial.println("INIT - SWITCH");
}

void run_switches(){
  
  boolean globe_switch = digitalRead(GLOBE_SWITCH);
  boolean sat_switch = digitalRead(SAT_SWITCH);
  boolean emergency_stop = digitalRead(EMERGENCY_STOP);

  if(!globe_switch && globe_switch_state){
    set_zero_globe();
  }
  
  if(!sat_switch && sat_switch_state){
    set_zero_sat();
  }
  
  if(!emergency_stop && emergency_stop_state){
    stop_all();
  }
  
  globe_switch_state = globe_switch;
  sat_switch_state = sat_switch;
  emergency_stop_state = emergency_stop;
  
}
