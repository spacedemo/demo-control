#ifndef MOTOR_H
#define MOTOR_H

#include "constant.hpp"

void setup_motor();

void run_motor();

void set_zero_globe();
void set_zero_sat();
void stop_all();

void globe_set_speed(int speed_step_s);
void sat_set_speed(int speed_step_s);
long globe_get_position();
long sat_get_position();

#endif
