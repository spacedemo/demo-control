

#include <Thread.h>
#include <ThreadController.h>

#include "constant.hpp"
#include "data.h"
#include "communication.h"
#include "motor.h"
#include "switches.h"
#include "control.h"


// ThreadController that will controll all threads
ThreadController controller = ThreadController();

Thread serialThread = Thread();
Thread motorThread = Thread();
Thread switchThread = Thread();
Thread controlThread = Thread();

void setup() {
  
  setup_comm();
  setup_switches();
  setup_motor();
  setup_control();

  serialThread.onRun(run_comm);
  serialThread.setInterval(50);
  
  switchThread.onRun(run_switches);
  switchThread.setInterval(200);

  controlThread.onRun(run_control);
  switchThread.setInterval(100);
  
  motorThread.onRun(run_motor);

  controller.add(&serialThread);
  controller.add(&switchThread); 
  controller.add(&controlThread); 
  controller.add(&motorThread); 
  
  Serial.flush();
}

void loop() {
  controller.run();
}
