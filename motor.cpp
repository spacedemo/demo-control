#include "motor.h"
#include <Arduino.h>
#include <AccelStepper.h>
#include <AFMotor.h>

// two stepper motors one on each port
AF_Stepper motor_globe(200, 1);
AF_Stepper motor_sat(200, 2);

void forwardstep_globe() {  
  motor_globe.onestep(FORWARD, SINGLE);
}
void backwardstep_globe() {  
  motor_globe.onestep(BACKWARD, SINGLE);
}
// wrappers for the second motor!
void forwardstep_sat() {  
  motor_sat.onestep(FORWARD, SINGLE);
}
void backwardstep_sat() {  
  motor_sat.onestep(BACKWARD, SINGLE);
}

AccelStepper stepper_globe(forwardstep_globe, backwardstep_globe);
AccelStepper stepper_sat(forwardstep_sat, backwardstep_sat);


void setup_motor(){
  stepper_globe.setMaxSpeed(MAX_SPEED_GLOBE_STEP_SEC);
  stepper_sat.setMaxSpeed(MAX_SPEED_SAT_STEP_SEC);
  stepper_globe.setAcceleration(100.0);
  stepper_sat.setAcceleration(100.0);
  
  stepper_globe.setSpeed(0.0);
  stepper_sat.setSpeed(0.0);
  
  Serial.println("INIT - MOTOR");
}

void run_motor(){
  stepper_globe.runSpeed();
  stepper_sat.runSpeed();
}

void set_zero_globe(){
  float speed = stepper_globe.speed();
  stepper_globe.setCurrentPosition(0);
  stepper_globe.setSpeed(speed);
}

void set_zero_sat(){
  float speed = stepper_sat.speed();
  stepper_sat.setCurrentPosition(0);
  stepper_sat.setSpeed(speed);
}

void stop_all(){
  stepper_globe.stop();
  stepper_sat.stop();
  stepper_globe.setSpeed(0);
  stepper_sat.setSpeed(0);
  stepper_globe.disableOutputs();
  stepper_sat.disableOutputs();
}


void globe_set_speed(int speed_step_s){
  stepper_globe.setSpeed(speed_step_s);
}
void sat_set_speed(int speed_step_s){
  stepper_sat.setSpeed(speed_step_s);
}

long globe_get_position(){
  stepper_globe.currentPosition();
}
long sat_get_position(){
  stepper_sat.currentPosition();
}
