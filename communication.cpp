#include "communication.h"
#include <Arduino.h>
#include "data.h"

char receivedChars[256]; // an array to store the received data
static byte ndx = 0;

void setup_comm(){
  Serial.begin(57600);
  Serial.println("INIT - COMM");
}

void run_comm(){
  boolean newData = false;
  if (Serial.available() > 0) {
    while (Serial.available() > 0 && newData == false) {
      char rc = Serial.read();
      if(rc == '\n'){
        newData = true;
        receivedChars[ndx] = 0;
      } else {
        receivedChars[ndx] = rc;
        ndx++;
      }
    }
    if (newData){
      execute_command(receivedChars);
      ndx = 0;
    }
  }
}

void execute_command( const char* command ){
  if( command[0] == 'T' ){
    // Set Timespamp :
    // FORMAT : "T 123456789"
    unsigned long timestamp = atol(&(command[2]));
    setTimestamp_ms(timestamp);
  }
  if( command[0] == 'G' ){
    // Get Timespamp :
    // FORMAT : "G"
    Serial.println(getTimestamp_ms());
  }
  if( command[0] == 'P' ){
    // Get Pong :
    // FORMAT : "PING"
    Serial.print("PONG");
  }
  if( command[0] == 'S' ){
    // Set Positon :
    // FORMAT : "S XXX.XXXX XXX.XXXX 123455667" : Longitude - latitude - timestamp
    float latitude_rad, longitude_rad;
    unsigned long timestamp;
    sscanf(command, "S %f %f %ld", &latitude_rad, &longitude_rad, &timestamp);
    pushPosition({
      latitude_rad,
      longitude_rad,
      timestamp
      });
  }
  if( command[0] == 'D' ){
    // Set Debug
  }
  if( command[0] == 'R' ){
    // Reset Position
  }
}
